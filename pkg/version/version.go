package version

import (
	"fmt"
)

var (
	appVersion string
	gitCommit  string
	gitBranch  string
)

func Print() {
	fmt.Printf("Version information\nVersion: %s, Commit %s, Branch, %s\n", GetVersion(), GetCommit(), GetBranch())
}

func GetCommit() string {
	return gitCommit
}
func GetRelease() string {
	return gitCommit
}
func GetVersion() string {
	return appVersion
}
func GetBranch() string {
	return gitBranch
}

func SetVersion(version string) {
	appVersion = version
}
func SetCommit(commit string) {
	gitCommit = commit
}
func SetBranch(branch string) {
	gitBranch = branch
}
