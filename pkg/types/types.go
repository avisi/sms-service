package types

type Config struct {
	Endpoint   string   `yaml:"endpoint"`
	Token      string   `yaml:"token"`
	Recipients []string `yaml:"recipients"`
	Originator string   `yaml:"originator"`
}

type SMS struct {
	Recipients string `json:"recipients"`
	Originator string `json:"originator"`
	Body       string `json:"body"`
}
