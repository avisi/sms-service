package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/avisi/sms-service/pkg/middleware"
)

// StartServer starts the http server with a router and graceful shutdown
func StartServer(r *mux.Router, wait time.Duration) {
	logger := logrus.StandardLogger()

	w := logger.Writer()
	defer w.Close()

	nextRequestID := func() string {
		return uuid.New().String()
	}

	srv := &http.Server{
		Handler:      middleware.Tracing(nextRequestID)(middleware.Logging(logger)(r)),
		Addr:         "0.0.0.0:8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		ErrorLog:     log.New(w, "", 0),
	}

	logger.Info("Starting server...")

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			logger.Error(err)
		}
	}()
	handleGracefulShutdown(srv, wait)
}

func handleGracefulShutdown(srv *http.Server, wait time.Duration) {
	// Handle graceful shutdown
	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	logrus.Info("shutting down")
	os.Exit(0)
}
