BINARY = sms-service
GOARCH = amd64

VERSION = '0.1'
COMMIT=$(shell git rev-parse HEAD)
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)

GITLAB_ORG=avisi
BUILD_DIR=${GOPATH}/src/gitlab.com/${GITLAB_ORG}/${BINARY}

PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

CURRENT_DIR=$(shell pwd)
BUILD_DIR_LINK=$(shell readlink ${BUILD_DIR})
LDFLAGS = -ldflags "-X main.Version=${VERSION} -X main.Commit=${COMMIT} -X main.Branch=${BRANCH}"

all: link clean linux darwin

linux:
	GOOS=linux GOARCH=${GOARCH} go build ${LDFLAGS} -o ${BINARY}-linux-${GOARCH} ./cmd/${BINARY} ;

darwin:
	GOOS=darwin GOARCH=${GOARCH} go build ${LDFLAGS} -o ${BINARY}-darwin-${GOARCH} ./cmd/${BINARY} ;

windows:
	GOOS=windows GOARCH=${GOARCH} go build ${LDFLAGS} -o ${BINARY}-windows-${GOARCH}.exe ./cmd/${BINARY} ;

build:
	go build ${LDFLAGS} -o ${BINARY} ./cmd/${BINARY} ;
	chmod +x ${BINARY};

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

fmt:
	@go fmt ${PKG_LIST};

install:
	dep ensure;

update-deps:
	dep ensure --vendor-only;

docker:
	docker build -t registry.gitlab.com/avisi/sms-service:${BRANCH} .

clean:
	-rm -f ${BINARY}-*

.PHONY: link linux darwin windows test vet fmt clean
