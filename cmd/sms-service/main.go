package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"io/ioutil"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/avisi/sms-service/pkg/server"
	"gitlab.com/avisi/sms-service/pkg/types"
	"gitlab.com/avisi/sms-service/pkg/version"
	yaml "gopkg.in/yaml.v2"
)

var (
	configFile   *string
	printVersion *bool

	config types.Config
)

var (
	Version string
	Commit  string
	Branch  string
)

func flags() {
	printVersion = flag.Bool("version", false, "Print version information")
	configFile = flag.String("config", "./config.yaml", "Path to the configuration file")

	flag.Parse()
}

func handleSMS(w http.ResponseWriter, r *http.Request) {
	message := r.URL.Query().Get("message")
	if message == "" {
		log.Info("No message given, will not send sms")
	}

	sms := types.SMS{
		Recipients: strings.Join(config.Recipients, ","),
		Originator: config.Originator,
		Body:       message,
	}

	sendSMS(&sms)

}

func sendSMS(sms *types.SMS) {

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(sms)

	request, _ := http.NewRequest(http.MethodPost, config.Endpoint, b)
	request.Header.Set("Authorization", fmt.Sprintf("AccessKey %s", config.Token))
	request.Header.Set("Accept", "application/json")

	httpClient := http.Client{}

	response, err := httpClient.Do(request)
	if err != nil {
		log.Errorf("failed to execute http request: %s", err)
		return
	}
	// Forbidden: After creating a realm, following GETs for the realm return 403 until you refresh
	if response.StatusCode == http.StatusUnauthorized || response.StatusCode == http.StatusForbidden {
		log.Debugf("[DEBUG] Response: %s.  Attempting refresh", response.Status)
	}
}

func main() {
	flags()
	initLoggingService()

	version.SetCommit(Commit)
	version.SetBranch(Branch)
	version.SetVersion(Version)

	if *printVersion {
		version.Print()
		return
	}

	configuration, err := readConfigFile()
	if err != nil {
		log.Errorf("failed to read config file: %s", err)
		log.Error("Exiting ...")
		os.Exit(1)
	}
	config = configuration

	r := mux.NewRouter()
	r.HandleFunc("/sms", handleSMS)

	server.StartServer(r, 10*time.Second)
}

func readConfigFile() (types.Config, error) {
	configuration := types.Config{}

	dat, err := ioutil.ReadFile(*configFile)
	if err != nil {
		return configuration, err
	}

	err = yaml.Unmarshal(dat, &configuration)
	if err != nil {
		return configuration, err
	}

	if token, ok := os.LookupEnv("API_TOKEN"); ok {
		configuration.Token = token
	}

	return configuration, nil
}

func initLoggingService() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)
}
