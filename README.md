# SMS Service

[![pipeline status](https://gitlab.com/avisi/sms-service/badges/master/pipeline.svg)](https://gitlab.com/avisi/sms-service/commits/master)

Simple http service to send sms's using a webhook and the messagebird API.


## Table of Contents

- [Usage](#usage)
- [Contribute](#contribute)
- [License](#license)

## Usage

```yaml
version: '2.4'

services:
    sms-service:
        image: registry.gitlab.com/avisi/sms-service:latest
        ports: ['8000:8000']
        volumes:
        - './config:/config.yaml'
```

## Contribute

PRs welcome. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/sms-service/issues).

## License

The code in this project is licensed under [Apache 2.0](LICENSE).
