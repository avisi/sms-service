FROM golang:1.11.4-alpine3.8 as builder
RUN apk add --update make curl git musl-dev gcc
RUN curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64 && chmod +x /usr/local/bin/dep
RUN mkdir -p /go/src/gitlab.com/avisi/sms-service

WORKDIR /go/src/gitlab.com/avisi/sms-service

COPY Gopkg.toml /go/src/gitlab.com/avisi/sms-service/Gopkg.toml
COPY Gopkg.lock /go/src/gitlab.com/avisi/sms-service/Gopkg.lock
RUN dep ensure -vendor-only

COPY . .

RUN make build

FROM alpine:3.8

RUN apk add --no-cache ca-certificates

COPY --from=builder /go/src/gitlab.com/avisi/sms-service/sms-service /usr/local/bin/sms-service

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/sms-service"]
